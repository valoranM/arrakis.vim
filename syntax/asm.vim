" Vim syntax file
" Language: RISC-V assembly (RV32IM)
" Maintainter: Valeran <mvalo@proton.me>
" Last Change: 2023 Oct 11

if exists("b:current_syntax")
  finish
endif

syn clear

syn keyword rvI addi xori ori andi slli srli sari slti jalr
syn keyword rvB beq bne blt bge bltu bgeu
syn keyword rvS ecall
syn keyword rvILoad lb lh lw lbu lhu
syn keyword rvJ jal
syn keyword rvR add sub xor or and sll srl sra slt sltu
syn keyword rvRM mul mulh mulhsu mulhu div divu rem remu
syn keyword rvS sb sh sw
syn keyword rvU lui auipc

syn keyword rvITR mv not neg seqz snez sltz sgtz
syn keyword rvRO beqz bnez blez bgez bltz bgtz
syn keyword rvPseudo nop li la j jal jr jalr ret call tail

syn region rvComment start="#" end="\$"

syn match rvReg "\<x\(\([0-2]\?[0-9]\?\)\|\(3[0-1]\)\)\>"

hi def link rvI       Keyword
hi def link rvB       Keyword
hi def link rvS       Keyword
hi def link rvILoad   Keyword
hi def link rvJ       Keyword
hi def link rvR       Keyword
hi def link rvRM      Keyword
hi def link rvS       Keyword
hi def link rvU       Keyword
hi def link rvITR     Keyword
hi def link rvRO      Keyword
hi def link rvComment Comment

let b:current_syntax = "rv"
